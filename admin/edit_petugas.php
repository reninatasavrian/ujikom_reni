<!doctype html>
<?php
include ('cek.php');
error_reporting(0);
?>
<?php
include "koneksi.php";
$id_petugas=$_GET['id_petugas'];

$select=mysql_query("select * from petugas  where id_petugas='$id_petugas'");
$data=mysql_fetch_array($select);
?>
<html class="no-js" lang="en">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
	<title>Aplikasi Sarana dan Prasarana di SMK</title>
    <meta name="description" content="Sufee Admin - HTML5 Admin Template">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link rel="apple-touch-icon" href="apple-icon.png">
    <link rel="shortcut icon" href="favicon.ico">
    <link rel="stylesheet" href="vendors/bootstrap/dist/css/bootstrap.min.css">
    <link rel="stylesheet" href="vendors/font-awesome/css/font-awesome.min.css">
    <link rel="stylesheet" href="vendors/themify-icons/css/themify-icons.css">
    <link rel="stylesheet" href="vendors/flag-icon-css/css/flag-icon.min.css">
    <link rel="stylesheet" href="vendors/selectFX/css/cs-skin-elastic.css">
    <link rel="stylesheet" href="vendors/datatables.net-bs4/css/dataTables.bootstrap4.min.css">
    <link rel="stylesheet" href="vendors/datatables.net-buttons-bs4/css/buttons.bootstrap4.min.css">

    <link rel="stylesheet" href="assets/css/style.css">
    <link rel="shortcut icon" href="logosmk.jpeg">
    <link href='https://fonts.googleapis.com/css?family=Open+Sans:400,600,700,800' rel='stylesheet' type='text/css'>
</head>

<body>
    <!-- Left Panel -->

    <aside id="left-panel" class="left-panel">
        <nav class="navbar navbar-expand-sm navbar-default">

                <?php 
			 if ($_SESSION['id_level']==1)
			{
			 echo '<div id="main-menu" class="main-menu collapse navbar-collapse">
                <ul class="nav navbar-nav">
                    <li class="menu-title"><a href="beranda.php"><p align="center"><b>~ Administrator ~</p></b></a></li>
                  <li><a href="inventaris.php"><i class="menu-icon fa fa-tasks"></i>Inventarisir </a></li>
				  <li><a href="peminjaman.php"> <i class="menu-icon ti-email"></i>Peminjaman </a></li>
				  <li><a href="pengembalian.php"> <i class="menu-icon fa fa-bar-chart"></i>Pengembalian </a></li>
				    <li class="menu-item-has-children dropdown">
                        <a href="#" class="dropdown-toggle" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false"> <i class="menu-icon fa fa-area-chart"></i>Master Data</a>
                        <ul class="sub-menu children dropdown-menu">
                            <li><i class="menu-icon fa fa-map-o"></i><a href="jenis.php">Jenis</a></li>
                            <li><i class="menu-icon fa fa-bank (alias)"></i><a href="ruang.php">Ruang</a></li>
                            <li><i class="menu-icon fa fa-male"></i><a href="petugas.php">Petugas</a></li>
                            <li><i class="menu-icon fa fa-group (alias)"></i><a href="pegawai.php">Pegawai</a></li>
                            <li><i class="menu-icon fa fa-newspaper-o"></i><a href="level.php">Level</a></li>
                        </ul>
                    </li>
					<li><a href="laporan.php"> <i class="menu-icon fa fa-tasks"></i>Laporan </a></li>
					<li><a href="index.php"> <i class="menu-icon fa fa-tasks"></i>Logout</a></li>
                </ul>
            </div><!-- /.navbar-collapse --><!--tutup menu-->';
			}
			 
			elseif ($_SESSION['id_level']==2){
				echo '<div id="main-menu" class="main-menu collapse navbar-collapse">
                <ul class="nav navbar-nav">
                  <li class="menu-title"><a href="beranda.php"><p align="center"><b>~ Operator ~</p></b></a></li>
				  <li><a href="peminjaman.php"> <i class="menu-icon ti-email"></i>Peminjaman </a></li>
				  <li><a href="pengembalian.php"> <i class="menu-icon fa fa-bar-chart"></i>Pengembalian </a></li>
					  <li><a href="index.php"> <i class="menu-icon fa fa-tasks"></i>Logout</a></li>
                </ul>
            </div><!-- /.navbar-collapse --><!--tutup menu-->';
			}
			
			elseif ($_SESSION['id_level']==3){
				echo '<div id="main-menu" class="main-menu collapse navbar-collapse">
                <ul class="nav navbar-nav">
                    <li class="menu-title"><a href="beranda.php"><p align="center"><b>~ Peminjaman ~</p></b></a></li>
				    <li><a href="peminjaman.php"> <i class="menu-icon ti-email"></i>Peminjaman </a></li>
					<li><a href="index.php"> <i class="menu-icon fa fa-tasks"></i>Logout</a></li>
                </ul>
            </div><!-- /.navbar-collapse --><!--tutup menu-->';
			}
			?>
			
        </nav>
    </aside>
   <div id="right-panel" class="right-panel">
         <header id="header" class="header">
            <div class="header-menu">
                <div class="col-sm-7">
                    <a id="menuToggle" class="menutoggle pull-left"><i class="fa fa fa-tasks"></i></a>
                </div>
                <div class="col-sm-5">
                    <div class="user-area dropdown float-right">
					<button type="button" class="btn btn-secondary mb-1" data-toggle="modal" data-target="#staticModal"> Logout</i></a></strong>
                    </div>
                </div>
            </div>
			</header>
			 <div class="modal fade" id="staticModal" tabindex="-1" role="dialog" aria-labelledby="staticModalLabel" aria-hidden="true" data-backdrop="static">
                    <div class="modal-dialog modal-sm" role="document">
                        <div class="modal-content">
                            <div class="modal-header">
                                <h5 class="modal-title" id="staticModalLabel">Logout</h5>
                                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                    <span aria-hidden="true">&times;</span>
                                </button>
                            </div>
                            <div class="modal-body">
                                <p>
                                   Apakah Anda Yakin ?
                                </p>
                            </div>
                            <div class="modal-footer">
                                <button type="button" class="btn btn-secondary" data-dismiss="modal">Cancel</button>
                              <a href="index.php">  <button type="button" class="btn btn-primary"><font color="white">Confirm</font></button></a>
                            </div>
                        </div>
                    </div>
                </div>
		
<div class="content mt-3">
            <div class="animated fadeIn">
                <div class="row">
				<div class="col-lg-12">
                            <div class="card-body">
									<div class="card-header">
									<strong size="6">Tabel Petugas</strong>
									</div>									 
									<div class="card-body card-block">
									<?php
									$id=$data['id_petugas'];
									$select = mysql_query("SELECT  a 
																	left join level on b.id_level=a.id_level
																	left join pegawai on b.id_pegawai=a.id_pegawai
																	where id_petugas='".$id."'
																	");
																	$select_level=mysql_query("select`id_level`,`nama` from `level`");
																	$select_pegawai=mysql_query("select`id_pegawai`,`nama_pegawai` from `pegawai`");
																	
																	?>
																	
                                         <form action="update_petugas.php?id_petugas=<?php echo $id_petugas; ?>" novalidate="novalidate">
                                            <div class="row form-group">
                                            <div class="col col-md-3">
											<label for="hf-email" class=" form-control-label">Nama Petugas</label></div>
                                            <div class="col-12 col-md-9"><input type="text" name="nama_petugas" class="form-control"value="<?php echo $data['nama_petugas'];?>"></div>
                                            </div>
											
											 <div class="row form-group">
                                            <div class="col col-md-3">
											<label for="hf-email" class=" form-control-label">Username</label></div>
                                            <div class="col-12 col-md-9"><input type="text" name="username" class="form-control"value="<?php echo $data['username'];?>"></div>
											<div class="col-12 col-md-9"><input type="hidden" name="id_petugas" class="form-control" value="<?php echo $data['id_petugas'];?>"></div>
                                            
                                            </div>
											<div class="row form-group">
                                            <div class="col col-md-3">
											<label for="hf-email" class=" form-control-label">Password</label></div>
                                            <div class="col-12 col-md-9"><input type="text" name="password" class="form-control"value="<?php echo $data['password'];?>"></div>
                                            </div>
											
											<div class="row form-group">
                                            <div class="col col-md-3">
											<label for="hf-email" class=" form-control-label">Level</label></div>
                                             <div class="col-12 col-md-9">
											<select name="id_level" class="form-control">
											<?php while($data_level=mysql_fetch_array($select_level)){?>
											<option value="<?php echo $data_level['id_level'];?>"
											<?php if($data['id_level']==$data_level['id_level']) {echo "selected"; }?>>
											<?php echo $data_level['nama']; }?>
											</option>
											</select>
											</div>
                                          </div>
										 <div class="row form-group">
                                            <div class="col col-md-3">
											<label for="hf-email" class=" form-control-label">Pegawai</label></div>
                                             <div class="col-12 col-md-9">
											<select name="id_pegawai" class="form-control">
											<?php while($data_pegawai=mysql_fetch_array($select_pegawai)){?>
											<option value="<?php echo $data_pegawai['id_pegawai'];?>"
											<?php if($data['id_pegawai']==$data_pegawai['id_pegawai']) {echo "selected"; }?>>
											<?php echo $data_pegawai['nama_pegawai']; }?>
											</option>
											</select>
											</div>
                                          </div>
                                          </div>
                                               <div class="card-footer">
                                                      <div class="form-group">
												<button id="send" class="btn btn-success " name="submit">Update </button>
												<input class="btn btn-danger " type="reset" name="reset" value="Reset" />
												</div>
                                            </div>
                                        </form>
                                    </div>
                                </div>

                            </div>

                    </div>
        </div> <!-- .content -->
    </div><!-- /#right-panel -->

    <!-- Right Panel -->
	
    <script src="vendors/jquery/dist/jquery.min.js"></script>
    <script src="vendors/popper.js/dist/umd/popper.min.js"></script>
    <script src="vendors/bootstrap/dist/js/bootstrap.min.js"></script>
    <script src="assets/js/main.js"></script>


    <script src="vendors/datatables.net/js/jquery.dataTables.min.js"></script>
    <script src="vendors/datatables.net-bs4/js/dataTables.bootstrap4.min.js"></script>
    <script src="vendors/datatables.net-buttons/js/dataTables.buttons.min.js"></script>
    <script src="vendors/datatables.net-buttons-bs4/js/buttons.bootstrap4.min.js"></script>
    <script src="vendors/jszip/dist/jszip.min.js"></script>
    <script src="vendors/pdfmake/build/pdfmake.min.js"></script>
    <script src="vendors/pdfmake/build/vfs_fonts.js"></script>
    <script src="vendors/datatables.net-buttons/js/buttons.html5.min.js"></script>
    <script src="vendors/datatables.net-buttons/js/buttons.print.min.js"></script>
    <script src="vendors/datatables.net-buttons/js/buttons.colVis.min.js"></script>
    <script src="assets/js/init-scripts/data-table/datatables-init.js"></script>


</body>

</html>
