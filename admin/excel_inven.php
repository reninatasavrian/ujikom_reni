<?php
// Load file koneksi.php
include "koneksi.php";
// Load plugin PHPExcel nya
require_once 'PHPExcel/PHPExcel.php';
// Panggil class PHPExcel nya
$excel = new PHPExcel();
// Settingan awal file excel
$excel->getProperties()->setCreator('My Notes Code')
             ->setLastModifiedBy('My Notes Code')
             ->setTitle("Data barang")
             ->setSubject("Inventaris")
             ->setDescription("Laporan Semua Data Inventaris")
             ->setKeywords("Data Inventaris");
// Buat sebuah variabel untuk menampung pengaturan style dari header tabel
$style_col = array(
  'font' => array('bold' => true), // Set font nya jadi bold
  'alignment' => array(
    'horizontal' => PHPExcel_Style_Alignment::HORIZONTAL_CENTER, // Set text jadi ditengah secara horizontal (center)
    'vertical' => PHPExcel_Style_Alignment::VERTICAL_CENTER // Set text jadi di tengah secara vertical (middle)
  ),
  'borders' => array(
    'top' => array('style'  => PHPExcel_Style_Border::BORDER_THIN), // Set border top dengan garis tipis
    'right' => array('style'  => PHPExcel_Style_Border::BORDER_THIN),  // Set border right dengan garis tipis
    'bottom' => array('style'  => PHPExcel_Style_Border::BORDER_THIN), // Set border bottom dengan garis tipis
    'left' => array('style'  => PHPExcel_Style_Border::BORDER_THIN) // Set border left dengan garis tipis
  )
);
// Buat sebuah variabel untuk menampung pengaturan style dari isi tabel
$style_row = array(
  'alignment' => array(
    'horizontal' => PHPExcel_Style_Alignment::HORIZONTAL_CENTER,// Set text jadi di tengah secara vertical (middle)
    'vertical' => PHPExcel_Style_Alignment::VERTICAL_CENTER // Set text jadi di tengah secara vertical (middle)
  ),
  'borders' => array(
    'top' => array('style'  => PHPExcel_Style_Border::BORDER_THIN), // Set border top dengan garis tipis
    'right' => array('style'  => PHPExcel_Style_Border::BORDER_THIN),  // Set border right dengan garis tipis
    'bottom' => array('style'  => PHPExcel_Style_Border::BORDER_THIN), // Set border bottom dengan garis tipis
    'left' => array('style'  => PHPExcel_Style_Border::BORDER_THIN) // Set border left dengan garis tipis
  )
);
$excel->setActiveSheetIndex(0)->setCellValue('A1', "Data Inventaris Barang"); // Set kolom A1 dengan tulisan "DATA BARANG"
$excel->getActiveSheet()->mergeCells('A1:F1'); // Set Merge Cell pada kolom A1 sampai F1
$excel->getActiveSheet()->getStyle('A1')->getFont()->setBold(TRUE); // Set bold kolom A1
$excel->getActiveSheet()->getStyle('A1')->getFont()->setSize(15); // Set font size 15 untuk kolom A1
$excel->getActiveSheet()->getStyle('A1')->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER); // Set text center untuk kolom A1
// Buat header tabel nya pada baris ke 3
$excel->setActiveSheetIndex(0)->setCellValue('A3', "No"); // Set kolom A3 dengan tulisan "NO"
$excel->setActiveSheetIndex(0)->setCellValue('B3', "Nama Barang"); // Set kolom B3 dengan tulisan "NAMA BARANG"
$excel->setActiveSheetIndex(0)->setCellValue('C3', "Kondisi"); // Set kolom C3 dengan tulisan "KUALITAS"
$excel->setActiveSheetIndex(0)->setCellValue('D3', "Keterangan"); // Set kolom D3 dengan tulisan "MERK"
$excel->setActiveSheetIndex(0)->setCellValue('E3', "Jumlah"); // Set kolom E3 dengan tulisan "HARGA"
$excel->setActiveSheetIndex(0)->setCellValue('F3', "Jenis"); // Set kolom F3 dengan tulisan "HARGA"
$excel->setActiveSheetIndex(0)->setCellValue('G3', "Tanggal Register"); // Set kolom F3 dengan tulisan "HARGA"
$excel->setActiveSheetIndex(0)->setCellValue('H3', "Ruang"); // Set kolom F3 dengan tulisan "HARGA"
$excel->setActiveSheetIndex(0)->setCellValue('I3', "Kode Barang"); // Set kolom F3 dengan tulisan "HARGA"
$excel->setActiveSheetIndex(0)->setCellValue('J3', "Petugas"); // Set kolom F3 dengan tulisan "HARGA"


// Apply style header yang telah kita buat tadi ke masing-masing kolom header
$excel->getActiveSheet()->getStyle('A3')->applyFromArray($style_col);
$excel->getActiveSheet()->getStyle('B3')->applyFromArray($style_col);
$excel->getActiveSheet()->getStyle('C3')->applyFromArray($style_col);
$excel->getActiveSheet()->getStyle('D3')->applyFromArray($style_col);
$excel->getActiveSheet()->getStyle('E3')->applyFromArray($style_col);
$excel->getActiveSheet()->getStyle('F3')->applyFromArray($style_col);
$excel->getActiveSheet()->getStyle('G3')->applyFromArray($style_col);
$excel->getActiveSheet()->getStyle('H3')->applyFromArray($style_col);
$excel->getActiveSheet()->getStyle('I3')->applyFromArray($style_col);
$excel->getActiveSheet()->getStyle('J3')->applyFromArray($style_col);
// Set height baris ke 1, 2 dan 3
$excel->getActiveSheet()->getRowDimension('1')->setRowHeight(20);
$excel->getActiveSheet()->getRowDimension('2')->setRowHeight(20);
$excel->getActiveSheet()->getRowDimension('3')->setRowHeight(20);
$excel->getActiveSheet()->getRowDimension('4')->setRowHeight(20);
$excel->getActiveSheet()->getRowDimension('5')->setRowHeight(20);
$excel->getActiveSheet()->getRowDimension('6')->setRowHeight(20);
$excel->getActiveSheet()->getRowDimension('7')->setRowHeight(20);
$excel->getActiveSheet()->getRowDimension('8')->setRowHeight(20);
$excel->getActiveSheet()->getRowDimension('9')->setRowHeight(20);
$excel->getActiveSheet()->getRowDimension('10')->setRowHeight(20);

// Buat query untuk menampilkan semua data siswa
$select=mysql_query("SELECT * FROM inventaris  a left join jenis b on b.id_jenis=a.id_jenis
														left join ruang c on c.id_ruang=a.id_ruang
														left join petugas d on d.id_petugas=a.id_petugas
														");

$no = 1; // Untuk penomoran tabel, di awal set dengan 1
$numrow =4 ; // Set baris pertama untuk isi tabel adalah baris ke 4
while($data=mysql_fetch_array($select)){
  $excel->setActiveSheetIndex(0)->setCellValue('A'.$numrow, $no);
  $excel->setActiveSheetIndex(0)->setCellValue('B'.$numrow, $data['nama']);
  $excel->setActiveSheetIndex(0)->setCellValue('C'.$numrow, $data['kondisi']);
  $excel->setActiveSheetIndex(0)->setCellValue('D'.$numrow, $data['keterangan']);
  $excel->setActiveSheetIndex(0)->setCellValue('E'.$numrow, $data['jumlah']);
  $excel->setActiveSheetIndex(0)->setCellValue('F'.$numrow, $data['nama_jenis']);
  $excel->setActiveSheetIndex(0)->setCellValue('G'.$numrow, $data['tanggal_register']);
  $excel->setActiveSheetIndex(0)->setCellValue('H'.$numrow, $data['nama_ruang']);
  $excel->setActiveSheetIndex(0)->setCellValue('I'.$numrow, $data['kode_inventaris']);
  $excel->setActiveSheetIndex(0)->setCellValue('J'.$numrow, $data['nama_petugas']);

  
  // Apply style row yang telah kita buat tadi ke masing-masing baris (isi tabel)
  $excel->getActiveSheet()->getStyle('A'.$numrow)->applyFromArray($style_row);
  $excel->getActiveSheet()->getStyle('B'.$numrow)->applyFromArray($style_row);
  $excel->getActiveSheet()->getStyle('C'.$numrow)->applyFromArray($style_row);
  $excel->getActiveSheet()->getStyle('D'.$numrow)->applyFromArray($style_row);
  $excel->getActiveSheet()->getStyle('E'.$numrow)->applyFromArray($style_row);  
  $excel->getActiveSheet()->getStyle('F'.$numrow)->applyFromArray($style_row);  
  $excel->getActiveSheet()->getStyle('G'.$numrow)->applyFromArray($style_row);    
  $excel->getActiveSheet()->getStyle('H'.$numrow)->applyFromArray($style_row);    
  $excel->getActiveSheet()->getStyle('I'.$numrow)->applyFromArray($style_row);    
  $excel->getActiveSheet()->getStyle('J'.$numrow)->applyFromArray($style_row);    
  
  $excel->getActiveSheet()->getRowDimension($numrow)->setRowHeight(20);
  
  $no++; // Tambah 1 setiap kali looping
  $numrow++; // Tambah 1 setiap kali looping
}
// Set width kolom
$excel->getActiveSheet()->getColumnDimension('A')->setWidth(10); // Set width kolom A
$excel->getActiveSheet()->getColumnDimension('B')->setWidth(15); // Set width kolom B
$excel->getActiveSheet()->getColumnDimension('C')->setWidth(25); // Set width kolom C
$excel->getActiveSheet()->getColumnDimension('D')->setWidth(20); // Set width kolom D
$excel->getActiveSheet()->getColumnDimension('E')->setWidth(20); // Set width kolom E
$excel->getActiveSheet()->getColumnDimension('F')->setWidth(16); // Set width kolom F
$excel->getActiveSheet()->getColumnDimension('G')->setWidth(20); // Set width kolom G
$excel->getActiveSheet()->getColumnDimension('H')->setWidth(20); // Set width kolom G
$excel->getActiveSheet()->getColumnDimension('I')->setWidth(20); // Set width kolom G
$excel->getActiveSheet()->getColumnDimension('J')->setWidth(20); // Set width kolom G

// Set orientasi kertas jadi LANDSCAPE
$excel->getActiveSheet()->getPageSetup()->setOrientation(PHPExcel_Worksheet_PageSetup::ORIENTATION_LANDSCAPE);
// Set judul file excel nya
$excel->getActiveSheet(0)->setTitle("Laporan Data Inventaris Barang");
$excel->setActiveSheetIndex(0);
// Proses file excel
header('Content-Type: application/vnd.openxmlformats-officedocument.spreadsheetml.sheet');
header('Content-Disposition: attachment; filename="Data Inventaris.xls"'); // Set nama file excel nya
header('Cache-Control: max-age=0');
$write = PHPExcel_IOFactory::createWriter($excel, 'Excel2007');
$write->save('php://output');
?>