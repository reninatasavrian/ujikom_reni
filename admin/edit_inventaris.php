<!doctype html>
<?php
include ('cek.php');
error_reporting(0);
?>

<?php
include "koneksi.php";
$id_inventaris=$_GET['id_inventaris'];
$select=mysql_query("select * from inventaris where id_inventaris='$id_inventaris'");
$data=mysql_fetch_array($select);
?>
<html class="no-js" lang="en">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
	<title>Aplikasi Sarana dan Prasarana di SMK</title>
    <meta name="description" content="Sufee Admin - HTML5 Admin Template">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link rel="apple-touch-icon" href="apple-icon.png">
    <link rel="shortcut icon" href="favicon.ico">
    <link rel="stylesheet" href="vendors/bootstrap/dist/css/bootstrap.min.css">
    <link rel="stylesheet" href="vendors/font-awesome/css/font-awesome.min.css">
    <link rel="stylesheet" href="vendors/themify-icons/css/themify-icons.css">
    <link rel="stylesheet" href="vendors/flag-icon-css/css/flag-icon.min.css">
    <link rel="stylesheet" href="vendors/selectFX/css/cs-skin-elastic.css">
    <link rel="stylesheet" href="vendors/datatables.net-bs4/css/dataTables.bootstrap4.min.css">
    <link rel="stylesheet" href="vendors/datatables.net-buttons-bs4/css/buttons.bootstrap4.min.css">

    <link rel="stylesheet" href="assets/css/style.css">
    <link rel="shortcut icon" href="logosmk.jpeg">
    <link href='https://fonts.googleapis.com/css?family=Open+Sans:400,600,700,800' rel='stylesheet' type='text/css'>
</head>

<body>
    <!-- Left Panel -->

    <aside id="left-panel" class="left-panel">
        <nav class="navbar navbar-expand-sm navbar-default">

              <?php 
			 if ($_SESSION['id_level']==1)
			{
			 echo '<div id="main-menu" class="main-menu collapse navbar-collapse">
                <ul class="nav navbar-nav">
                    <li class="menu-title"><a href="beranda.php"><p align="center"><b>~ Administrator ~</p></b></a></li>
                  <li><a href="inventaris.php"><i class="menu-icon fa fa-tasks"></i>Inventarisir </a></li>
				  <li><a href="peminjaman.php"> <i class="menu-icon ti-email"></i>Peminjaman </a></li>
				  <li><a href="pengembalian.php"> <i class="menu-icon fa fa-bar-chart"></i>Pengembalian </a></li>
				    <li class="menu-item-has-children dropdown">
                        <a href="#" class="dropdown-toggle" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false"> <i class="menu-icon fa fa-area-chart"></i>Master Data</a>
                        <ul class="sub-menu children dropdown-menu">
                            <li><i class="menu-icon fa fa-map-o"></i><a href="jenis.php">Jenis</a></li>
                            <li><i class="menu-icon fa fa-bank (alias)"></i><a href="ruang.php">Ruang</a></li>
                            <li><i class="menu-icon fa fa-male"></i><a href="petugas.php">Petugas</a></li>
                            <li><i class="menu-icon fa fa-group (alias)"></i><a href="pegawai.php">Pegawai</a></li>
                            <li><i class="menu-icon fa fa-newspaper-o"></i><a href="level.php">Level</a></li>
                        </ul>
                    </li>
					<li><a href="laporan.php"> <i class="menu-icon fa fa-tasks"></i>Laporan </a></li>
                </ul>
            </div><!-- /.navbar-collapse --><!--tutup menu-->';
			}
			 
		elseif ($_SESSION['id_level']==2){
				echo '<div id="main-menu" class="main-menu collapse navbar-collapse">
                <ul class="nav navbar-nav">
                  <li class="menu-title"><a href="beranda.php"><p align="center"><b>~ Operator ~</p></b></a></li>
				  <li><a href="peminjaman.php"> <i class="menu-icon ti-email"></i>Peminjaman </a></li>
				  <li><a href="pengembalian.php"> <i class="menu-icon fa fa-bar-chart"></i>Pengembalian </a></li>
                </ul>
            </div><!-- /.navbar-collapse --><!--tutup menu-->';
			}
			
			elseif ($_SESSION['id_level']==3){
				echo '<div id="main-menu" class="main-menu collapse navbar-collapse">
                <ul class="nav navbar-nav">
                    <li class="menu-title"><a href="beranda.php"><p align="center"><b>~ Peminjaman ~</p></b></a></li>
				    <li><a href="peminjaman.php"> <i class="menu-icon ti-email"></i>Peminjaman </a></li>
                </ul>
            </div><!-- /.navbar-collapse --><!--tutup menu-->';
			}
			?>
			
        </nav>
    </aside>
    <div id="right-panel" class="right-panel">
      
			
	
		
<div class="content mt-3">
            <div class="animated fadeIn">
                <div class="row">
				<div class="col-lg-12">
                            <div class="card-body">
									<div class="card-header">
									<strong size="6">Tabel Inventaris</strong>
									</div>									 
									<div class="card-body card-block">
									<?php
									$id=$data['id_inventaris'];
									$select = mysql_query("SELECT  a 
																	left join jenis on b.id_jenis=a.id_jenis
																	left join ruang on b.id_ruang=a.id_ruang
																	left join petugas on b.id_petugas=a.id_petugas
																	where id_inventaris='".$id."'
																	");
																	$select_jenis=mysql_query("select`id_jenis`,`nama_jenis` from `jenis`");
																	$select_ruang=mysql_query("select`id_ruang`,`nama_ruang` from `ruang`");
																	$select_petugas=mysql_query("select`id_petugas`,`nama_petugas` from `petugas`");
																	
																	?>
																	
									
                                        <form action="update_inventaris.php?id_inventaris=<?php echo $id_inventaris; ?>" method="post" novalidate="novalidate">
                                            
											<div class="row form-group">
                                            <div class="col col-md-3">
											<label for="hf-email" class=" form-control-label">Nama Barang</label></div>
                                            <div class="col-12 col-md-9"><input type="text" name="nama" maxlength="30" class="form-control" value="<?php echo $data['nama'];?>"></div>
                                            </div>
											<div class="row form-group">
                                            <div class="col col-md-3">
											<label for="hf-email" class=" form-control-label">Kondisi</label></div>
                                            <div class="col-12 col-md-9"><input type="text" name="kondisi" maxlength="20" class="form-control" value="<?php echo $data['kondisi'];?>"></div>
                                            </div>
											<div class="row form-group">
                                            <div class="col col-md-3">
											<label for="hf-email" class=" form-control-label">Keterangan</label></div>
                                            <div class="col-12 col-md-9"><input type="text" name="keterangan" maxlength="20" class="form-control"value="<?php echo $data['keterangan'];?>"></div>
                                            </div>
											<div class="row form-group">
                                            <div class="col col-md-3">
											<label for="hf-email" class=" form-control-label">Jumlah</label></div>
                                            <div class="col-12 col-md-9"><input type="number" name="jumlah" class="form-control"value="<?php echo $data['jumlah'];?>"></div>
                                            </div>
											<div class="row form-group">
                                            <div class="col col-md-3">
											<label for="hf-email" class=" form-control-label">Nama Jenis</label></div>
                                            <div class="col-12 col-md-9">
											<select name="id_jenis" class="form-control">
											<?php while($data_jenis=mysql_fetch_array($select_jenis)){?>
											<option value="<?php echo $data_jenis['id_jenis'];?>"
											<?php if($data['id_jenis']==$data_jenis['nama_jenis']) { echo "selected"; }?>>
											<?php echo $data_jenis['nama_jenis']; }?>
											</option>
											
											</select>
											</div>
                                          </div>
											<div class="row form-group">
                                            <div class="col col-md-3">
											<label for="hf-email" class=" form-control-label">Tanggal Register</label></div>
                                            <div class="col-12 col-md-9"><input type="date" name="tanggal_register" class="form-control" value="<?php echo $data['tanggal_register'];?>"></div>
                                            </div>
											<div class="row form-group">
                                            <div class="col col-md-3">
											<label for="hf-email" class=" form-control-label">Nama Ruang</label></div>
                                             <div class="col-12 col-md-9">
											<select name="id_ruang" class="form-control">
											<?php while($data_ruang=mysql_fetch_array($select_ruang)){?>
											<option value="<?php echo $data_ruang['id_ruang'];?>"
											<?php if($data['id_ruang']==$data_ruang['id_ruang']) {echo "selected"; }?>>
											<?php echo $data_ruang['nama_ruang']; }?>
											</option>
											</select>
											</div>
                                          </div>
											<div class="row form-group">
                                            <div class="col col-md-3">
											<label for="hf-email" class=" form-control-label">Kode Inventaris</label></div>
                                            <div class="col-12 col-md-9"><input type="text" id="disabled-input" name="kode_inventaris" class="form-control" value="<?php echo $data['kode_inventaris'];?>" readonly>
                                            <small class="help-block form-text"><font color="green">*auto</font></small></div></div>
                                            
											<div class="row form-group">
                                            <div class="col col-md-3">
											<label for="hf-email" class=" form-control-label">Nama Petugas</label></div>
                                             <div class="col-12 col-md-9">
											<select name="id_petugas" class="form-control">
											<?php while($data_petugas=mysql_fetch_array($select_petugas)){?>
											<option value="<?php echo $data_petugas['id_petugas'];?>"
											<?php if($data['id_petugas']==$data_petugas['id_petugas']) {echo "selected"; }?>>
											<?php echo $data_petugas['nama_petugas']; }?>
											</option>
											</select>
											</div>
                                          </div>
                                          </div>
                                               <div class="card-footer">
                                                      <div class="form-group">
												<button id="send" class="btn btn-primary " name="submit">Update </button>
												<input class="btn btn-danger " type="reset" name="reset" value="Reset" />
												</div>
                                            </div>
                                        </form>
                                    </div>
                                </div>

                            </div>

                    </div>
        </div> <!-- .content -->
    </div><!-- /#right-panel -->

    <!-- Right Panel -->
	
    <script src="vendors/jquery/dist/jquery.min.js"></script>
    <script src="vendors/popper.js/dist/umd/popper.min.js"></script>
    <script src="vendors/bootstrap/dist/js/bootstrap.min.js"></script>
    <script src="assets/js/main.js"></script>


    <script src="vendors/datatables.net/js/jquery.dataTables.min.js"></script>
    <script src="vendors/datatables.net-bs4/js/dataTables.bootstrap4.min.js"></script>
    <script src="vendors/datatables.net-buttons/js/dataTables.buttons.min.js"></script>
    <script src="vendors/datatables.net-buttons-bs4/js/buttons.bootstrap4.min.js"></script>
    <script src="vendors/jszip/dist/jszip.min.js"></script>
    <script src="vendors/pdfmake/build/pdfmake.min.js"></script>
    <script src="vendors/pdfmake/build/vfs_fonts.js"></script>
    <script src="vendors/datatables.net-buttons/js/buttons.html5.min.js"></script>
    <script src="vendors/datatables.net-buttons/js/buttons.print.min.js"></script>
    <script src="vendors/datatables.net-buttons/js/buttons.colVis.min.js"></script>
    <script src="assets/js/init-scripts/data-table/datatables-init.js"></script>


</body>

</html>
