<!doctype html>
<?php
include ('cek.php');
error_reporting(0);
?>
<html class="no-js" lang="en">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
	<title>Aplikasi Sarana dan Prasarana di SMK</title>
    <meta name="description" content="Sufee Admin - HTML5 Admin Template">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link rel="apple-touch-icon" href="apple-icon.png">
    <link rel="shortcut icon" href="favicon.ico">
    <link rel="stylesheet" href="vendors/bootstrap/dist/css/bootstrap.min.css">
    <link rel="stylesheet" href="vendors/font-awesome/css/font-awesome.min.css">
    <link rel="stylesheet" href="vendors/themify-icons/css/themify-icons.css">
    <link rel="stylesheet" href="vendors/flag-icon-css/css/flag-icon.min.css">
    <link rel="stylesheet" href="vendors/selectFX/css/cs-skin-elastic.css">
    <link rel="stylesheet" href="vendors/datatables.net-bs4/css/dataTables.bootstrap4.min.css">
    <link rel="stylesheet" href="vendors/datatables.net-buttons-bs4/css/buttons.bootstrap4.min.css">

    <link rel="stylesheet" href="assets/css/style.css">
    <link rel="shortcut icon" href="logosmk.jpeg">
    <link href='https://fonts.googleapis.com/css?family=Open+Sans:400,600,700,800' rel='stylesheet' type='text/css'>
</head>

<body>
    <!-- Left Panel -->

    <aside id="left-panel" class="left-panel">
        <nav class="navbar navbar-expand-sm navbar-default">
 <?php 
			 if ($_SESSION['id_level']==1)
			{
			 echo '<div id="main-menu" class="main-menu collapse navbar-collapse">
                <ul class="nav navbar-nav">
                    <li class="menu-title"><a href="beranda.php"><p align="center"><b>~ Administrator ~</p></b></a></li>
                  <li><a href="inventaris.php"><i class="menu-icon fa fa-tasks"></i>Inventarisir </a></li>
				  <li><a href="peminjaman.php"> <i class="menu-icon ti-email"></i>Peminjaman </a></li>
				  <li><a href="pengembalian.php"> <i class="menu-icon fa fa-bar-chart"></i>Pengembalian </a></li>
				    <li class="menu-item-has-children dropdown">
                        <a href="#" class="dropdown-toggle" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false"> <i class="menu-icon fa fa-area-chart"></i>Master Data</a>
                        <ul class="sub-menu children dropdown-menu">
                            <li><i class="menu-icon fa fa-map-o"></i><a href="jenis.php">Jenis</a></li>
                            <li><i class="menu-icon fa fa-bank (alias)"></i><a href="ruang.php">Ruang</a></li>
                            <li><i class="menu-icon fa fa-male"></i><a href="petugas.php">Petugas</a></li>
                            <li><i class="menu-icon fa fa-group (alias)"></i><a href="pegawai.php">Pegawai</a></li>
                            <li><i class="menu-icon fa fa-newspaper-o"></i><a href="level.php">Level</a></li>
                        </ul>
                    </li>
					<li><a href="laporan.php"> <i class="menu-icon fa fa-tasks"></i>Laporan </a></li>
                </ul>
            </div><!-- /.navbar-collapse --><!--tutup menu-->';
			}
			 
			elseif ($_SESSION['id_level']==2){
				echo '<div id="main-menu" class="main-menu collapse navbar-collapse">
                <ul class="nav navbar-nav">
                  <li class="menu-title"><a href="beranda.php"><p align="center"><b>~ Operator ~</p></b></a></li>
				  <li><a href="peminjaman.php"> <i class="menu-icon ti-email"></i>Peminjaman </a></li>
				  <li><a href="pengembalian.php"> <i class="menu-icon fa fa-bar-chart"></i>Pengembalian </a></li>
                </ul>
            </div><!-- /.navbar-collapse --><!--tutup menu-->';
			}
			
			elseif ($_SESSION['id_level']==3){
				echo '<div id="main-menu" class="main-menu collapse navbar-collapse">
                <ul class="nav navbar-nav">
                    <li class="menu-title"><a href="beranda.php"><p align="center"><b>~ Peminjaman ~</p></b></a></li>
				    <li><a href="peminjaman.php"> <i class="menu-icon ti-email"></i>Peminjaman </a></li>
                </ul>
            </div><!-- /.navbar-collapse --><!--tutup menu-->';
			}
			?>
			
        </nav>
</aside>
 <div id="right-panel" class="right-panel">
         <header id="header" class="header">
            <div class="header-menu">
                <div class="col-sm-7">
                    <a id="menuToggle" class="menutoggle pull-left"><i class="fa fa fa-tasks"></i></a>
                </div>
                <div class="col-sm-5">
                    <div class="user-area dropdown float-right">
					<button type="button" class="btn btn-secondary mb-1" data-toggle="modal" data-target="#staticModal"> Logout</i></a></strong>
                    </div>
                </div>
            </div>
			</header>
			 <div class="modal fade" id="staticModal" tabindex="-1" role="dialog" aria-labelledby="staticModalLabel" aria-hidden="true" data-backdrop="static">
                    <div class="modal-dialog modal-sm" role="document">
                        <div class="modal-content">
                            <div class="modal-header">
                                <h5 class="modal-title" id="staticModalLabel">Logout</h5>
                                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                    <span aria-hidden="true">&times;</span>
                                </button>
                            </div>
                            <div class="modal-body">
                                <p>
                                   Apakah Anda Yakin ?
                                </p>
                            </div>
                            <div class="modal-footer">
                                <button type="button" class="btn btn-secondary" data-dismiss="modal">Cancel</button>
                              <a href="index.php">  <button type="button" class="btn btn-primary"><font color="white">Confirm</font></button></a>
                            </div>
                        </div>
                    </div>
                </div>
			<div class="breadcrumbs">
				<div class="alert alert-success" role="alert">
					<p align="center"> <font size="5"color="black">Laporan Berdasarkan Tanggal </font></p>
				</div><br/>
				 <div class="col-sm-4">
                <div class="page-header float-left">
                </div>
            </div>
		
		   <div class="row">
            <div class="col-lg-12">
                        <div class="position-center">
						<div class="col-lg-5">
						<label><b>Pilih Tanggal : </b></label>
							<form method="get">
											<select name="tanggal" class="form-control m-bot15">
												<?php
												include "koneksi.php";
												//display values in combobox/dropdown
												$result = mysql_query("SELECT day(tanggal_pinjam) as tanggal FROM peminjaman GROUP BY DAY(tanggal_pinjam)");
												while($row = mysql_fetch_assoc($result))
												{
												echo "<option value='$row[tanggal]'>$row[tanggal]</option>";
												} 
												?>
											</select>
													<br/>
												<button type="submit" class="btn btn-outline btn-primary">Tampilkan</button>
											</form>
											<br/>
											<br/>
						</div>
					</div>
	</div>
</div>
        </div>

 <?php if(isset($_GET['tanggal'])) { ?>
                            <div class="card-body bg-white">
                                <table id="bootstrap-data-table-export" class="table table-striped table-bordered">
                                    <thead> 
														 <tr>
															<th>No</th>
															<th>Kode Pinjam</th>
															<th>Nama Barang</th>
															<th>Jumlah Pinjam</th>
															<th>Tanggal Pinjam</th>
															<th>Tanggal Kembali</th>
															<th>Nama Pegawai</th>
												
															</tr> 
											</thead>   
                                    <tbody>
                                   <?php
									$no=1;
										$select=mysql_query("select * from peminjaman a left join pegawai b on b.id_pegawai=a.id_pegawai
																left join detail_pinjam d on a.id_peminjaman=d.id_detail_pinjam
																left join inventaris i on i.id_inventaris=d.id_inventaris WHERE DAY(tanggal_pinjam) = '$_GET[tanggal]'");
									
												while($data=mysql_fetch_array($select))
												{
												?>
                                       <tr>
													<td><?php echo $no++; ?></td>
													<td><?php echo $data['kode_pinjam']; ?></td>
													<td><?php echo $data['nama']; ?></td>
													<td><?php echo $data['jumlah']; ?></td>
													<td><?php echo $data['tanggal_pinjam']; ?></td>
													<td><?php echo $data['tanggal_kembali']; ?></td>
													<td><?php echo $data['nama_pegawai']; ?></td>
													</tr>
										<?php	
									}
										?>
                                     </tbody>
									</table><br/>
									
									 <form method="POST" action="pdf_tanggal.php"></td>
								 <input type="hidden" name="tanggal" value="<?php echo $_GET['tanggal']?>">
								 <button type="submit" class="btn btn-danger">Cetak PDF </button></form>
					
							<br/>
							<br/>
							<br/>
							<br/>
									   </div>
									   </div>
                     
									<?php
										}
									?>
	

    <!-- Right Panel -->
	
    <script src="vendors/jquery/dist/jquery.min.js"></script>
	
    <script src="vendors/popper.js/dist/umd/popper.min.js"></script>
    <script src="vendors/bootstrap/dist/js/bootstrap.min.js"></script>
    <script src="assets/js/main.js"></script>


    <script src="vendors/datatables.net/js/jquery.dataTables.min.js"></script>
    <script src="vendors/datatables.net-bs4/js/dataTables.bootstrap4.min.js"></script>
    <script src="vendors/datatables.net-buttons/js/dataTables.buttons.min.js"></script>
    <script src="vendors/datatables.net-buttons-bs4/js/buttons.bootstrap4.min.js"></script>
    <script src="vendors/jszip/dist/jszip.min.js"></script>
    <script src="vendors/pdfmake/build/pdfmake.min.js"></script>
    <script src="vendors/pdfmake/build/vfs_fonts.js"></script>
    <script src="vendors/datatables.net-buttons/js/buttons.html5.min.js"></script>
    <script src="vendors/datatables.net-buttons/js/buttons.print.min.js"></script>
    <script src="vendors/datatables.net-buttons/js/buttons.colVis.min.js"></script>
    <script src="assets/js/init-scripts/data-table/datatables-init.js"></script>



</body>

</html>
