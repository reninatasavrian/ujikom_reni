
<!doctype html>

<html class="no-js" lang="en">
<!--<![endif]-->

<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <title>Aplikasi Inventaris Sarana dan Prasarana di SMK</title>
    <meta name="description" content="Sufee Admin - HTML5 Admin Template">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <link rel="apple-touch-icon" href="apple-icon.png">
    <link rel="shortcut icon" href="favicon.ico">


    <link rel="stylesheet" href="vendors/bootstrap/dist/css/bootstrap.min.css">
    <link rel="stylesheet" href="vendors/font-awesome/css/font-awesome.min.css">
    <link rel="stylesheet" href="vendors/themify-icons/css/themify-icons.css">
    <link rel="stylesheet" href="vendors/flag-icon-css/css/flag-icon.min.css">
    <link rel="stylesheet" href="vendors/selectFX/css/cs-skin-elastic.css">

    <link rel="stylesheet" href="assets/css/style.css">

    <link href='https://fonts.googleapis.com/css?family=Open+Sans:400,600,700,800' rel='stylesheet' type='text/css'>



</head>


<body class="bg-dark">
              <div class="login-content">
                <div class="login-form">
				<a><p align="center"><font size="6"><b>Please Sign In</b></p></font></a> 	
				<br> 
                  <form action="cek_login.php" method="post">	
                        <div class="form-group">
                            <input type="username" name="username" class="form-control" placeholder="Username" required=""/>
                        </div>
                            <div class="form-group">
                                <input type="password" name="password" class="form-control" placeholder="Password" required=""/>
                        </div>
                                <button type="submit" class="btn btn-danger btn-flat m-b-30 m-t-30">Login</button>
                   </form><br/>
					 <hr/>
					 <div class="register-link m-t-15 text-center">
                      <p>Don't have account ? <a href="#"> Sign Up Here</a></p>
                     </div>
            </div>
            </div>
        
    
    <script src="vendors/jquery/dist/jquery.min.js"></script>
    <script src="vendors/popper.js/dist/umd/popper.min.js"></script>
    <script src="vendors/bootstrap/dist/js/bootstrap.min.js"></script>
    <script src="assets/js/main.js"></script>


</body>

</html>

		