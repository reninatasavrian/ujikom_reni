<!doctype html>
<?php
include ('cek.php');
error_reporting(0);
?>
<html class="no-js" lang="en">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
	<title>Aplikasi Sarana dan Prasarana di SMK</title>
    <meta name="description" content="Sufee Admin - HTML5 Admin Template">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link rel="apple-touch-icon" href="apple-icon.png">
    <link rel="shortcut icon" href="favicon.ico">
    <link rel="stylesheet" href="vendors/bootstrap/dist/css/bootstrap.min.css">
    <link rel="stylesheet" href="vendors/font-awesome/css/font-awesome.min.css">
    <link rel="stylesheet" href="vendors/themify-icons/css/themify-icons.css">
    <link rel="stylesheet" href="vendors/flag-icon-css/css/flag-icon.min.css">
    <link rel="stylesheet" href="vendors/selectFX/css/cs-skin-elastic.css">
    <link rel="stylesheet" href="vendors/datatables.net-bs4/css/dataTables.bootstrap4.min.css">
    <link rel="stylesheet" href="vendors/datatables.net-buttons-bs4/css/buttons.bootstrap4.min.css">

    <link rel="stylesheet" href="assets/css/style.css">
    <link rel="shortcut icon" href="logosmk.jpeg">
    <link href='https://fonts.googleapis.com/css?family=Open+Sans:400,600,700,800' rel='stylesheet' type='text/css'>
</head>

<body>
    <!-- Left Panel -->

    <aside id="left-panel" class="left-panel">
        <nav class="navbar navbar-expand-sm navbar-default">

             <?php 
			 if ($_SESSION['id_level']==1)
			{
			 echo '<div id="main-menu" class="main-menu collapse navbar-collapse">
                <ul class="nav navbar-nav">
                    <li class="menu-title"><a href="beranda.php"><p align="center"><b>~ Administrator ~</p></b></a></li>
                  <li><a href="inventaris.php"><i class="menu-icon fa fa-tasks"></i>Inventarisir </a></li>
				  <li><a href="peminjaman.php"> <i class="menu-icon ti-email"></i>Peminjaman </a></li>
				  <li><a href="pengembalian.php"> <i class="menu-icon fa fa-bar-chart"></i>Pengembalian </a></li>
				    <li class="menu-item-has-children dropdown">
                        <a href="#" class="dropdown-toggle" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false"> <i class="menu-icon fa fa-area-chart"></i>Master Data</a>
                        <ul class="sub-menu children dropdown-menu">
                            <li><i class="menu-icon fa fa-map-o"></i><a href="jenis.php">Jenis</a></li>
                            <li><i class="menu-icon fa fa-bank (alias)"></i><a href="ruang.php">Ruang</a></li>
                            <li><i class="menu-icon fa fa-male"></i><a href="petugas.php">Petugas</a></li>
                            <li><i class="menu-icon fa fa-group (alias)"></i><a href="pegawai.php">Pegawai</a></li>
                            <li><i class="menu-icon fa fa-newspaper-o"></i><a href="level.php">Level</a></li>
                        </ul>
                    </li>
					<li><a href="laporan.php"> <i class="menu-icon fa fa-tasks"></i>Laporan </a></li>
					
                </ul>
            </div><!-- /.navbar-collapse --><!--tutup menu-->';
			}
			 
			elseif ($_SESSION['id_level']==2){
				echo '<div id="main-menu" class="main-menu collapse navbar-collapse">
                <ul class="nav navbar-nav">
                  <li class="menu-title"><a href="beranda.php"><p align="center"><b>~ Operator ~</p></b></a></li>
				  <li><a href="peminjaman.php"> <i class="menu-icon ti-email"></i>Peminjaman </a></li>
				  <li><a href="pengembalian.php"> <i class="menu-icon fa fa-bar-chart"></i>Pengembalian </a></li>
                </ul>
            </div><!-- /.navbar-collapse --><!--tutup menu-->';
			}
			
			elseif ($_SESSION['id_level']==3){
				echo '<div id="main-menu" class="main-menu collapse navbar-collapse">
                <ul class="nav navbar-nav">
                    <li class="menu-title"><a href="beranda.php"><p align="center"><b>~ Peminjaman ~</p></b></a></li>
				    <li><a href="peminjaman.php"> <i class="menu-icon ti-email"></i>Peminjaman </a></li>
                </ul>
            </div><!-- /.navbar-collapse --><!--tutup menu-->';
			}
			?>
			
        </nav>
    </aside>
    <div id="right-panel" class="right-panel">
         <header id="header" class="header">

            <div class="header-menu">

                <div class="col-sm-7">
                    <a id="menuToggle" class="menutoggle pull-left"><i class="fa fa fa-tasks"></i></a>
                </div>

                <div class="col-sm-5">
                    <div class="user-area dropdown float-right">
 <button type="button" class="btn btn-secondary mb-1" data-toggle="modal" data-target="#staticModal"> Logout</i></a></strong>
		
                    </div>
                </div>
            </div>

        </header><!-- /header -->
			 <div class="modal fade" id="staticModal" tabindex="-1" role="dialog" aria-labelledby="staticModalLabel" aria-hidden="true" data-backdrop="static">
                    <div class="modal-dialog modal-sm" role="document">
                        <div class="modal-content">
                            <div class="modal-header">
                                <h5 class="modal-title" id="staticModalLabel">Logout</h5>
                                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                    <span aria-hidden="true">&times;</span>
                                </button>
                            </div>
                            <div class="modal-body">
                                <p>
                                   Apakah Anda Yakin ?
                                </p>
                            </div>
                            <div class="modal-footer">
                                <button type="button" class="btn btn-secondary" data-dismiss="modal">Cancel</button>
                               <a href="index.php"> <button type="button" class="btn btn-primary"><font color="white">Confirm</font></button></a>
                            </div>
                        </div>
                    </div>
                </div>
				
		 <div class="breadcrumbs">
            <div class="col-sm-4">
                <div class="page-header float-left">
                    <div class="page-title">
                        <h1>Home</h1>
                    </div>
                </div>
            </div>
            <div class="col-sm-8">
                <div class="page-header float-right">
                    <div class="page-title">
                        <ol class="breadcrumb text-right">
                            <li class="active">Home</li>
                        </ol>
                    </div>
                </div>
            </div>
        </div><br/><br/>
				<div class="col-sm-12">
                <div class="alert  alert-success alert-dismissible fade show" role="alert">
                    <font size="5"><span class="badge badge-pill badge-success">Berhasil Login!!</span>&nbsp; Selamat Datang di Aplikasi Sarana dan Prasarana di SMK.</font>
                    <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
            </div>
			
				<!-- /# column -->
                    <div class="col-lg-12">
                        <div class="card">
                            <div class="card-header">
                                <h4 >Aplikasi Sarana dan Prasarana </h4>
                            </div>
                            <div class="card-body">
                                <div class="custom-tab">

                                    <nav>
                                        <div class="nav nav-tabs" id="nav-tab" role="tablist">
                                            <a class="nav-item nav-link active" id="custom-nav-home-tab" data-toggle="tab" href="#custom-nav-home" role="tab" aria-controls="custom-nav-home" aria-selected="true">Pengertian</a>
                                            <a class="nav-item nav-link" id="custom-nav-profile-tab" data-toggle="tab" href="#custom-nav-profile" role="tab" aria-controls="custom-nav-profile" aria-selected="false">Tujuan</a>
                                            <a class="nav-item nav-link" id="custom-nav-contact-tab" data-toggle="tab" href="#custom-nav-contact" role="tab" aria-controls="custom-nav-contact" aria-selected="false">Manfaat</a>
                                        </div>
                                    </nav>
                                    <div class="tab-content pl-3 pt-2" id="nav-tabContent">
                                        <div class="tab-pane fade show active" id="custom-nav-home" role="tabpanel" aria-labelledby="custom-nav-home-tab">
                                            <p align="justify"><b>Inventaris Sarana dan Prasarana di sekolah adalah pencatatan atau pendaftaran barang-barang milik sekolah kedalam suatu daftar inventaris barang secara tertib dan teratur ketentuan dan tata cara yang berlaku.
											 Barang inventaris sekolah adalah semua barang milik negara(yang dikuasai sekolah) baik yang diadakan atau dibeli melalui dana dari pemerintah,DPP maupun 
											 diperoleh sebagai pertukaran,hadiah atau hiabh serta hasil usaha pembuatan sendiri disekolah guna menunjang kelancaran proses belajar mengajar.</b> </p>
                                        </div>
                                        <div class="tab-pane fade" id="custom-nav-profile" role="tabpanel" aria-labelledby="custom-nav-profile-tab">
                                            <p align="justify"><b>1. Untuk menjaga pendataan sarana dan prasarana yang dimiliki oleh suatu sekolah.<br/>
											   2. Untuk memudahkan pengawasan dan pengendalian sarana dan prasarana yang dimiliki oleh suatu sekolah.<br/>
											   3. Untuk mengehemat keuangan sekolah baik dalam pengadaan maupun untuk pemeliharaan dan pengahapusan sarana dan prasarana sekolah.</b></div>
                                         <div class="tab-pane fade" id="custom-nav-contact" role="tabpanel" aria-labelledby="custom-nav-contacttab">
                                            <p align="justify"><b>1. Menyediakan data dan informasi dalam rangka menentukan kebutuhan dan menyusun rencana kebutuhan barang.<br/>
											   2. Memberikan data dan informasi untuk dijadikan bahan/pedoman dalam pengarahan pengadaaan barang.<br/>
											   3. Memberikan data dan informasi dalam menentukan keadaan barang sebagai dasar untuk menetapkan penghapusannya.</b></div>
                                       
                                </div>
                            </div>
                        </div>
                    </div>
                    <!-- /# column -->

		
		
    <!-- Right Panel -->
	
    <script src="vendors/jquery/dist/jquery.min.js"></script>
    <script src="vendors/popper.js/dist/umd/popper.min.js"></script>
    <script src="vendors/bootstrap/dist/js/bootstrap.min.js"></script>
    <script src="assets/js/main.js"></script>


    <script src="vendors/datatables.net/js/jquery.dataTables.min.js"></script>
    <script src="vendors/datatables.net-bs4/js/dataTables.bootstrap4.min.js"></script>
    <script src="vendors/datatables.net-buttons/js/dataTables.buttons.min.js"></script>
    <script src="vendors/datatables.net-buttons-bs4/js/buttons.bootstrap4.min.js"></script>
    <script src="vendors/jszip/dist/jszip.min.js"></script>
    <script src="vendors/pdfmake/build/pdfmake.min.js"></script>
    <script src="vendors/pdfmake/build/vfs_fonts.js"></script>
    <script src="vendors/datatables.net-buttons/js/buttons.html5.min.js"></script>
    <script src="vendors/datatables.net-buttons/js/buttons.print.min.js"></script>
    <script src="vendors/datatables.net-buttons/js/buttons.colVis.min.js"></script>
    <script src="assets/js/init-scripts/data-table/datatables-init.js"></script>


</body>

</html>
