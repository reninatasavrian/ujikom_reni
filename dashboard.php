<!doctype html>
<!--[if lt IE 7]>      <html class="no-js lt-ie9 lt-ie8 lt-ie7" lang=""> <![endif]-->
<!--[if IE 7]>         <html class="no-js lt-ie9 lt-ie8" lang=""> <![endif]-->
<!--[if IE 8]>         <html class="no-js lt-ie9" lang=""> <![endif]-->
<!--[if gt IE 8]><!-->
<html class="no-js" lang="en">
<!--<![endif]-->

<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
	<title>Aplikasi Sarana dan Prasarana di SMK</title>
    <meta name="description" content="Sufee Admin - HTML5 Admin Template">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <link rel="apple-touch-icon" href="apple-icon.png">
    <link rel="shortcut icon" href="favicon.ico">

    <link rel="stylesheet" href="vendors/bootstrap/dist/css/bootstrap.min.css">
    <link rel="stylesheet" href="vendors/font-awesome/css/font-awesome.min.css">
    <link rel="stylesheet" href="vendors/themify-icons/css/themify-icons.css">
    <link rel="stylesheet" href="vendors/flag-icon-css/css/flag-icon.min.css">
    <link rel="stylesheet" href="vendors/selectFX/css/cs-skin-elastic.css">
    <link rel="stylesheet" href="vendors/jqvmap/dist/jqvmap.min.css">


    <link rel="stylesheet" href="assets/css/style.css">

    <link href='https://fonts.googleapis.com/css?family=Open+Sans:400,600,700,800' rel='stylesheet' type='text/css'>

</head>

<body>


 <aside id="left-panel" class="left-panel">
        <nav class="navbar navbar-expand-sm navbar-default"><br/>
				
		<div id="main-menu" class="main-menu collapse navbar-collapse">
                <ul class="nav navbar-nav">
                  <li><a href="dashboard.php"><i class="menu-icon fa fa-tasks"></i>Dashboard </a></li>
				  <li><a href="index.php"> <i class="menu-icon ti-email"></i>Login </a></li>
				
                </ul>
            </div><!-- /.navbar-collapse --><!--tutup menu-->';
			
			 </aside>
    <!-- Left Panel -->

    <!-- Right Panel -->

    <div id="right-panel" class="right-panel">

        <!-- Header-->
        <header id="header" class="header">
            <div class="header-menu">
                <div class="col-sm-7">
                    <a id="menuToggle" class="menutoggle pull-left"><i class="fa fa fa-tasks"></i></a>
        </header><!-- /header -->
        <!-- Header-->


         <div class="breadcrumbs">
            <div class="col-10">
               
				&nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp;<p align="center">
					&nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp;
						&nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp;
						<img  height="200" width="300" src="images/inventaris.jpg"/></p><br/>
                  </div>
                  
				   
          
				  <div class="col-lg-12">
                        <div class="">
                            <div class="card-body">
                                <div class="custom-tab">

                                    <nav>
                                        <div class="nav nav-tabs" id="nav-tab" role="tablist">
                                            <a class="nav-item nav-link active" id="custom-nav-home-tab" data-toggle="tab" href="#custom-nav-home" role="tab" aria-controls="custom-nav-home" aria-selected="true">Pengertian</a>
                                            <a class="nav-item nav-link" id="custom-nav-profile-tab" data-toggle="tab" href="#custom-nav-profile" role="tab" aria-controls="custom-nav-profile" aria-selected="false">Tujuan</a>
                                            <a class="nav-item nav-link" id="custom-nav-contact-tab" data-toggle="tab" href="#custom-nav-contact" role="tab" aria-controls="custom-nav-contact" aria-selected="false">Manfaat</a>
                                        </div>
                                    </nav>
                                    <div class="tab-content pl-3 pt-2" id="nav-tabContent">
                                        <div class="tab-pane fade show active" id="custom-nav-home" role="tabpanel" aria-labelledby="custom-nav-home-tab">
                                            <p align="justify"><b>Inventaris Sarana dan Prasarana di sekolah adalah pencatatan atau pendaftaran barang-barang milik sekolah kedalam suatu daftar inventaris barang secara tertib dan teratur ketentuan dan tata cara yang berlaku.
											 Barang inventaris sekolah adalah semua barang milik negara(yang dikuasai sekolah) baik yang diadakan atau dibeli melalui dana dari pemerintah,DPP maupun 
											 diperoleh sebagai pertukaran,hadiah atau hiabh serta hasil usaha pembuatan sendiri disekolah guna menunjang kelancaran proses belajar mengajar.</b> </p>
                                        </div>
                                        <div class="tab-pane fade" id="custom-nav-profile" role="tabpanel" aria-labelledby="custom-nav-profile-tab">
                                            <p align="justify"><b>1. Untuk menjaga pendataan sarana dan prasarana yang dimiliki oleh suatu sekolah.<br/>
											   2. Untuk memudahkan pengawasan dan pengendalian sarana dan prasarana yang dimiliki oleh suatu sekolah.<br/>
											   3. Untuk mengehemat keuangan sekolah baik dalam pengadaan maupun untuk pemeliharaan dan pengahapusan sarana dan prasarana sekolah.</b></div>
                                         <div class="tab-pane fade" id="custom-nav-contact" role="tabpanel" aria-labelledby="custom-nav-contacttab">
                                            <p align="justify"><b>1. Menyediakan data dan informasi dalam rangka menentukan kebutuhan dan menyusun rencana kebutuhan barang.<br/>
											   2. Memberikan data dan informasi untuk dijadikan bahan/pedoman dalam pengarahan pengadaaan barang.<br/>
											   3. Memberikan data dan informasi dalam menentukan keadaan barang sebagai dasar untuk menetapkan penghapusannya.</b></div>
                                       
                                </div>
                            </div>
                        </div>
						<br/>
						<br/>
						<br/>
						<br/>
						<br/>
					
                 </div>
                    </div>
					
                    <!-- /# column -->

		
            
            
           

		 

    <script src="vendors/jquery/dist/jquery.min.js"></script>
    <script src="vendors/popper.js/dist/umd/popper.min.js"></script>
    <script src="vendors/bootstrap/dist/js/bootstrap.min.js"></script>
    <script src="assets/js/main.js"></script>


    <script src="vendors/chart.js/dist/Chart.bundle.min.js"></script>
    <script src="assets/js/dashboard.js"></script>
    <script src="assets/js/widgets.js"></script>
    <script src="vendors/jqvmap/dist/jquery.vmap.min.js"></script>
    <script src="vendors/jqvmap/examples/js/jquery.vmap.sampledata.js"></script>
    <script src="vendors/jqvmap/dist/maps/jquery.vmap.world.js"></script>
    <script>
        (function($) {
            "use strict";

            jQuery('#vmap').vectorMap({
                map: 'world_en',
                backgroundColor: null,
                color: '#ffffff',
                hoverOpacity: 0.7,
                selectedColor: '#1de9b6',
                enableZoom: true,
                showTooltip: true,
                values: sample_data,
                scaleColors: ['#1de9b6', '#03a9f5'],
                normalizeFunction: 'polynomial'
            });
        })(jQuery);
    </script>

</body>

</html>
