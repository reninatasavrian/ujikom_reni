<!doctype html>
<?php
include ('cek.php');
error_reporting(0);
?>
<html class="no-js" lang="en">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
	<title>Aplikasi Sarana dan Prasarana di SMK</title>
    <meta name="description" content="Sufee Admin - HTML5 Admin Template">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link rel="apple-touch-icon" href="apple-icon.png">
    <link rel="shortcut icon" href="favicon.ico">
    <link rel="stylesheet" href="vendors/bootstrap/dist/css/bootstrap.min.css">
    <link rel="stylesheet" href="vendors/font-awesome/css/font-awesome.min.css">
    <link rel="stylesheet" href="vendors/themify-icons/css/themify-icons.css">
    <link rel="stylesheet" href="vendors/flag-icon-css/css/flag-icon.min.css">
    <link rel="stylesheet" href="vendors/selectFX/css/cs-skin-elastic.css">
    <link rel="stylesheet" href="vendors/datatables.net-bs4/css/dataTables.bootstrap4.min.css">
    <link rel="stylesheet" href="vendors/datatables.net-buttons-bs4/css/buttons.bootstrap4.min.css">

    <link rel="stylesheet" href="assets/css/style.css">
    <link rel="shortcut icon" href="logosmk.jpeg">
    <link href='https://fonts.googleapis.com/css?family=Open+Sans:400,600,700,800' rel='stylesheet' type='text/css'>
</head>

<body>
    <!-- Left Panel -->

  <aside id="left-panel" class="left-panel">
        <nav class="navbar navbar-expand-sm navbar-default">
 <?php 
			 if ($_SESSION['id_level']==1)
			{
			 echo '<div id="main-menu" class="main-menu collapse navbar-collapse">
                <ul class="nav navbar-nav">
                    <li class="menu-title"><a href="beranda.php"><p align="center"><b>~ Administrator ~</p></b></a></li>
                  <li><a href="inventaris.php"><i class="menu-icon fa fa-tasks"></i>Inventarisir </a></li>
				  <li><a href="peminjaman.php"> <i class="menu-icon ti-email"></i>Peminjaman </a></li>
				  <li><a href="pengembalian.php"> <i class="menu-icon fa fa-bar-chart"></i>Pengembalian </a></li>
				    <li class="menu-item-has-children dropdown">
                        <a href="#" class="dropdown-toggle" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false"> <i class="menu-icon fa fa-area-chart"></i>Master Data</a>
                        <ul class="sub-menu children dropdown-menu">
                            <li><i class="menu-icon fa fa-map-o"></i><a href="jenis.php">Jenis</a></li>
                            <li><i class="menu-icon fa fa-bank (alias)"></i><a href="ruang.php">Ruang</a></li>
                            <li><i class="menu-icon fa fa-male"></i><a href="petugas.php">Petugas</a></li>
                            <li><i class="menu-icon fa fa-group (alias)"></i><a href="pegawai.php">Pegawai</a></li>
                            <li><i class="menu-icon fa fa-newspaper-o"></i><a href="level.php">Level</a></li>
                        </ul>
                    </li>
					<li><a href="laporan.php"> <i class="menu-icon fa fa-tasks"></i>Laporan </a></li>
                </ul>
            </div><!-- /.navbar-collapse --><!--tutup menu-->';
			}
			 
			elseif ($_SESSION['id_level']==2){
				echo '<div id="main-menu" class="main-menu collapse navbar-collapse">
                <ul class="nav navbar-nav">
                  <li class="menu-title"><a href="beranda.php"><p align="center"><b>~ Operator ~</p></b></a></li>
				  <li><a href="peminjaman.php"> <i class="menu-icon ti-email"></i>Peminjaman </a></li>
				  <li><a href="pengembalian.php"> <i class="menu-icon fa fa-bar-chart"></i>Pengembalian </a></li>
                </ul>
            </div><!-- /.navbar-collapse --><!--tutup menu-->';
			}
			
			elseif ($_SESSION['id_level']==3){
				echo '<div id="main-menu" class="main-menu collapse navbar-collapse">
                <ul class="nav navbar-nav">
                    <li class="menu-title"><a href="beranda.php"><p align="center"><b>~ Peminjaman ~</p></b></a></li>
				    <li><a href="peminjaman.php"> <i class="menu-icon ti-email"></i>Peminjaman </a></li>
                </ul>
            </div><!-- /.navbar-collapse --><!--tutup menu-->';
			}
			?>
			
        </nav>
</aside>
 <div id="right-panel" class="right-panel">
         <header id="header" class="header">
            <div class="header-menu">
                <div class="col-sm-7">
                    <a id="menuToggle" class="menutoggle pull-left"><i class="fa fa fa-tasks"></i></a>
                </div>
                <div class="col-sm-5">
                    <div class="user-area dropdown float-right">
					<button type="button" class="btn btn-secondary mb-1" data-toggle="modal" data-target="#staticModal"> Logout</i></a></strong>
                    </div>
                </div>
            </div>
			</header>
			 <div class="modal fade" id="staticModal" tabindex="-1" role="dialog" aria-labelledby="staticModalLabel" aria-hidden="true" data-backdrop="static">
                    <div class="modal-dialog modal-sm" role="document">
                        <div class="modal-content">
                            <div class="modal-header">
                                <h5 class="modal-title" id="staticModalLabel">Logout</h5>
                                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                    <span aria-hidden="true">&times;</span>
                                </button>
                            </div>
                            <div class="modal-body">
                                <p>
                                   Apakah Anda Yakin ?
                                </p>
                            </div>
                            <div class="modal-footer">
                                <button type="button" class="btn btn-secondary" data-dismiss="modal">Cancel</button>
                              <a href="index.php">  <button type="button" class="btn btn-primary"><font color="white">Confirm</font></button></a>
                            </div>
                        </div>
                    </div>
                </div>
 
<div class="content mt-3">
            <div class="animated fadeIn">
                <div class="row">
                    <div class="col-md-12">
                        <div class="card">
						<div class="card-header">
                        <strong class="card-title"><font size="5">Data Pengembalian</font></strong>
                                       
						</div>
						
                            <div class="card-body">
                               <div class="row">
   <div class="col-lg-12">
                <section class="panel">
                    <div class="panel-body">
                        <div class="position-center">
						<div class="col-lg-5">
						<label>Pilih Kode Peminjaman</label>
							<form method="POST">
							<select name="id_peminjaman" class="form-control m-bot15">
							<?php
								include "koneksi.php";
								//display  values in combobox/dropdown
								$result = mysql_query("SELECT id_peminjaman,kode_pinjam FROM peminjaman where status_peminjaman='pinjam' ");
								while($row = mysql_fetch_assoc($result))
								{
								echo "<option value='$row[id_peminjaman]'>$row[kode_pinjam]</option>";
								} 
								?>
							</select>
									<br/>
								<button type="submit" name="pilih" class="btn btn-outline btn-primary">Tampilkan</button>
							</form>
						</div>
					</div>
				</div>
		</section>
	</div>
</div>
<div class="content mt-3">
            <div class="animated fadeIn">
                <div class="row">
                    <div class="col-md-12">
                            <div class="card-body">
					<?php
                if(isset($_POST['pilih'])){?>
                  <form action="proses_pengembalian.php" method="post" enctype="multipart/form-data">
                     <?php
                     include "koneksi.php";
                     $id_peminjaman=$_POST['id_peminjaman'];
                     $select=mysql_query("select * from peminjaman left join detail_pinjam on peminjaman.id_peminjaman=detail_pinjam.id_detail_pinjam
																	left join inventaris on inventaris.id_inventaris=detail_pinjam.id_inventaris
																	left join pegawai on pegawai.id_pegawai=peminjaman.id_pegawai
																	where id_peminjaman='$id_peminjaman'");                    
						while($data=mysql_fetch_array($select)){
                        ?>
				</div>
				<div class="row form-group">
                 <div class="col col-md-3">
					<label for="hf-email" class=" form-control-label">Nama Barang</label></div>
                     <div class="col-12 col-md-9">
                    <input name="id_peminjaman" type="hidden" class="form-control" placeholder="Masukan Jumlah barang" value="<?php echo $data['id_peminjaman'];?>" autocomplete="off" maxlength="11" required="">
                    <input name="id_detail_pinjam" type="hidden" class="form-control" placeholder="Masukan Jumlah barang" value="<?php echo $data['id_detail_pinjam'];?>" autocomplete="off" maxlength="11" required="">
                    <input name="id_inventaris" type="text" class="form-control"  placeholder="Masukan ID Inventaris" value="<?php echo $data['id_inventaris'];?>.<?php echo $data['nama'];?>" autocomplete="off" maxlength="11" required="" readonly>
				</div>
				</div>
               
                <div class="row form-group"> 
				 <div class="col col-md-3">
                    <label for="hf-email" class=" form-control-label">Tanggal Pinjam</label></div>
                   <div class="col-12 col-md-9">
                        <input type="text" name="tanggal_pinjam" value="<?php echo $data['tanggal_pinjam'];?>" class="form-control col-md-7 col-xs-12"readonly>
                        <input type="hidden" name="tanggal_kembali" class="form-control col-md-7 col-xs-12" readonly>
                    </div>
                </div>
				
				<div class="row form-group">
				 <div class="col col-md-3">
                    <label for="hf-email" class=" form-control-label">Nama Pegawai</label></div>
                   <div class="col-12 col-md-9">
                        <input type="datetime" name="id_pegawai" value="<?php echo $data['id_pegawai']?>.<?php echo $data['nama_pegawai']?>" class="form-control col-md-7 col-xs-12" placeholder=""readonly>
                    </div>
                </div>
				
				<div class="row form-group">
				 <div class="col col-md-3">
                    <label for="hf-email" class=" form-control-label">Jumlah</label></div>
                   <div class="col-12 col-md-9">
                        <input type="number" name="jumlah" value="<?php echo $data['jumlah_pinjam']?>" class="form-control col-md-7 col-xs-12" placeholder="nama"readonly>
                    </div>
                </div>
				
				<div class="row form-group">
				 <div class="col col-md-3">
                    <label for="hf-email" class=" form-control-label">Status Peminjaman</label></div>
                   <div class="col-12 col-md-9">
                       <select name="status_peminjaman" class="form-control m-bot15" readonly>
							<option>Kembali</option>
						</select>
					</div>
                </div>
			
           <button type="submit" class="btn btn-success">Kembalikan</button><br/>
            
    <?php } ?>
</form>

<?php } ?>
 </div>
         <div class="content mt-3">
            <div class="animated fadeIn">
                <div class="row">
                    <div class="col-md-12">
                        <div class="">
						
 <div class="card-body-12">
                                <table id="bootstrap-data-table-export" class="table table-striped table-bordered">
                                    <thead>
                                          <tr class="headings bg-dark">
												<th><font color="white">No</font></th>
												<th><font color="white">Tanggal Pinjam</font></th>
												<th><font color="white">Tanggal Kembali</font></th>
												<th><font color="white">Status Peminjaman</font></th>
												<th><font color="white">Nama Pegawai</font></th>
												
                                            </tr>
                                        </thead>
                                    <tbody>
                                     <?php
									include "koneksi.php";
									$no=1;
									$select=mysql_query("select * from peminjaman left join pegawai on peminjaman.id_pegawai=pegawai.id_pegawai where status_peminjaman='Kembali'");
									while($data=mysql_fetch_array($select))
									{
										?>
                                      <font face="Hobo std">  <tr class="succes">
                                            <td><?php echo $no++ ?></td>
                                            <td><?php echo $data['tanggal_pinjam']; ?></td>
                                            <td><?php echo $data['tanggal_kembali']; ?></td>
                                            <td><?php echo $data['status_peminjaman']; ?></td>
                                            <td><?php echo $data['nama_pegawai']; ?></td>
											  </font> </tr>
										<?php	
									}
										?>
                                    </tbody>
									</table>
									    </div>
                        </div>
                    </div>


                </div>
            </div><!-- .animated -->
        </div><!-- .content -->
    </div><!-- /#right-panel -->

	
    <script src="vendors/jquery/dist/jquery.min.js"></script>
    <script src="vendors/popper.js/dist/umd/popper.min.js"></script>
    <script src="vendors/bootstrap/dist/js/bootstrap.min.js"></script>
    <script src="assets/js/main.js"></script>


    <script src="vendors/datatables.net/js/jquery.dataTables.min.js"></script>
    <script src="vendors/datatables.net-bs4/js/dataTables.bootstrap4.min.js"></script>
    <script src="vendors/datatables.net-buttons/js/dataTables.buttons.min.js"></script>
    <script src="vendors/datatables.net-buttons-bs4/js/buttons.bootstrap4.min.js"></script>
    <script src="vendors/jszip/dist/jszip.min.js"></script>
    <script src="vendors/pdfmake/build/pdfmake.min.js"></script>
    <script src="vendors/pdfmake/build/vfs_fonts.js"></script>
    <script src="vendors/datatables.net-buttons/js/buttons.html5.min.js"></script>
    <script src="vendors/datatables.net-buttons/js/buttons.print.min.js"></script>
    <script src="vendors/datatables.net-buttons/js/buttons.colVis.min.js"></script>
    <script src="assets/js/init-scripts/data-table/datatables-init.js"></script>


</body>

</html>
